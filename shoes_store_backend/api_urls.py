from rest_framework import routers
from stores.api import StoreViewSet
from articles.api import ArticleViewSet,ArticleStoreViewSet

router = routers.DefaultRouter()

router.register(r'articles/stores',ArticleStoreViewSet)
router.register(r'articles', ArticleViewSet)
router.register(r'stores', StoreViewSet)
