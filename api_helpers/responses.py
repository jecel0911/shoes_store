from rest_framework.response import Response

def parameter_validate(pk):
	if not pk.isdigit():
		raise Exception('Bad request')

def list_response(serializer,size,model):
	return Response({model: serializer.data,'success': 'true','total_elements': size})

def retrieve_response(serializer,model):
    return Response({model: serializer.data,'success': 'true'})