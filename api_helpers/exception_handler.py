from rest_framework.views import exception_handler
from rest_framework.response import Response

def custom_exception_handler(exc, context):
    err_message = str(exc)
    err_code = 500

    print(exc)
    if exc is not None:
    	if str(exc) in "Bad request":
        	err_message = str(exc)
        	err_code = 400
        if str(exc) in "Invalid username/password." or 'credentials were not provided' in str(exc):
        	err_message = str(exc)
        	err_code = 401
        if "matching query does not exist." in str(exc):
        	err_message = "Record not found"
        	err_code = 404

    #print('custom-exception:'+str(exc))
    return Response({"error_msg": err_message,"error_code": err_code, "success": "false"})