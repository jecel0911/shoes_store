from django.contrib import admin
from .models import *

class ArticleAdmin(admin.ModelAdmin):
	list_display = ('id','store','name','description','price','total_in_shelf','total_in_vault',)
	list_filter = ('store',)
	search_fields = ('name','description',)
	ordering = ('store','name','description',)

admin.site.register(Article,ArticleAdmin)