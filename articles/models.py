from __future__ import unicode_literals
from django.db import models
from stores.models import Store

class Article(models.Model):
	store = models.ForeignKey(Store)
	name = models.CharField(max_length=100)
	description = models.TextField(max_length=256)
	price = models.DecimalField(max_digits=8, decimal_places=2)
	total_in_shelf = models.PositiveIntegerField(default=0)
	total_in_vault = models.PositiveIntegerField(default=0)

	def get_store_name(self):
		return self.store.name
		
	def __unicode__(self):
		return self.name

	def __str__(self):
		return self.__unicode__()