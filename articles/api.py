from django.conf.urls import url, include
from rest_framework import filters
from rest_framework.response import Response
from .models import Article
from stores.models import Store
from rest_framework import routers, serializers, viewsets
from api_helpers.responses import parameter_validate,list_response,retrieve_response

# Serializers define the API representation.
class ArticleSerializer(serializers.HyperlinkedModelSerializer):
    store_name = serializers.CharField(source='get_store_name')

    class Meta:
        model = Article
        fields = ('id','name','description','price','total_in_shelf','total_in_vault','store_name',)


# Service for Articles
class ArticleViewSet(viewsets.ReadOnlyModelViewSet):
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name','description',)

    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

    def list(self, request):
    	queryset = Article.objects.all()
    	serializer = ArticleSerializer(queryset, many=True)
        return list_response(serializer,len(queryset),'articles')

    def retrieve(self, request,pk=None):
    	parameter_validate(pk)
    	article = Article.objects.get(pk=pk)
    	serializer = ArticleSerializer(article, many=False)
    	return retrieve_response(serializer,'article')


# Service for articles in specific store
class ArticleStoreViewSet(viewsets.ReadOnlyModelViewSet):
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name','description',)

    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

    def list(self, request):
    	#retrieve the list is not a valid request
    	raise Exception('Bad request')

    def retrieve(self, request,pk=None):
    	parameter_validate(pk)
    	db_store = Store.objects.get(pk=pk)
    	articles = Article.objects.filter(store=db_store)
    	serializer = ArticleSerializer(articles, many=True)
    	return list_response(serializer,len(articles),'articles')

