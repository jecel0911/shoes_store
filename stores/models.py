from __future__ import unicode_literals

from django.db import models

class Store(models.Model):
	name = models.CharField(max_length=100)
	address = models.TextField(max_length=256)

	def __unicode__(self):
		return self.name

	def __str__(self):
		return self.__unicode__()