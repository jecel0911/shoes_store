from django.conf.urls import url, include
from rest_framework import filters
from rest_framework.response import Response
from .models import Store
from rest_framework import routers, serializers, viewsets
from api_helpers.responses import parameter_validate,list_response,retrieve_response


# Serializers define the API representation.
class StoreSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Store
        fields = ('id','name','address',)

# ViewSets define the view behavior.
class StoreViewSet(viewsets.ReadOnlyModelViewSet):
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name',)

    queryset = Store.objects.all()
    serializer_class = StoreSerializer

    def list(self, request):
    	queryset = Store.objects.all()
    	serializer = StoreSerializer(queryset, many=True)
        return list_response(serializer,len(queryset),'stores')

    def retrieve(self, request,pk=None):
    	parameter_validate(pk)
    	store = Store.objects.get(pk=pk)
    	serializer = StoreSerializer(store, many=False)
    	return retrieve_response(serializer,'store')
	
