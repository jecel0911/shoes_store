Feature: CRUD Store
    As a logged-in user
    I want to be able to test CRUD in Store catalog

    Scenario: Add New Store
        Given a user
        Then I can log in the admin site
        Then I can navigate to the new Store page
        
        When I add a new Store
        Then I'm redirected to the list of stores