from behave import *
from selenium.webdriver.common.keys import Keys

@then("I can log in the admin site")
def impl(context):
    context.browser.get(context.uri+'/admin/')
    context.browser.find_element_by_name('username').send_keys(context.user_name)
    context.browser.find_element_by_name('password').send_keys(context.password)
    context.browser.find_element_by_name('password').send_keys(Keys.RETURN)
    assert context.uri+'/admin/' == context.browser.current_url
	
@then("I can navigate to the new Store page")
def impl(context):
    context.browser.get(context.uri+'/admin/stores/store/add/')
    assert context.uri+'/admin/stores/store/add/' == context.browser.current_url

@when("I add a new Store")
def impl(context):
    context.browser.find_element_by_name('name').send_keys('test_stores')
    context.browser.find_element_by_name('address').send_keys('test_stores_address')
    context.browser.find_element_by_name('name').send_keys(Keys.RETURN)

@then("I'm redirected to the list of stores")
def impl(context):
    assert context.uri+'/admin/stores/store/' == context.browser.current_url


