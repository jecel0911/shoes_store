from behave import *
import requests

@given('a user')
def impl(context):
    pass

@then('I can consume the API')
def impl(context):
    pass

r = None

@when('I invoke the stores list with credentials')
def impl(context):
	r = requests.get("%s/services/stores" % (context.uri,), auth=(context.user_name, context.password))
	assert r.status_code == 200
	r_parsed = r.json()
	if(r_parsed['success'] == 'false'):
		raise Exception('API_ERROR, err_code:'+ str(r_parsed['error_code'])+', error_msg: ' + r_parsed['error_msg'])
	context.r = r

@then('I get the list of stores in the system')
def impl(context):
	r_parsed = context.r.json()
	if r_parsed['total_elements'] <> len(r_parsed['stores']):
		raise Exception('API_ERROR, total_elements different from quatity of elements')
