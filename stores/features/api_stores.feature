Feature: API Stores
    As a logged-in user
    I want to be able to see all stores in the system
    So that I can find and filter an specific store

    Scenario: retrieve the list of stores
        Given a user
        Then I can consume the API
        
        When I invoke the stores list with credentials
        Then I get the list of stores in the system