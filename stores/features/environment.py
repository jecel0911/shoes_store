# -- FILE: features/environment.py
from selenium import webdriver

def before_all(context):
	context.browser = webdriver.Firefox()
	context.user_name = 'test_user'
	context.password = 'test12345'
	context.uri = "http://localhost:8000"

def after_all(context):
    context.browser.quit()
    context.browser = None
    pass