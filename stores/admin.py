from django.contrib import admin
from .models import *

class StoreAdmin(admin.ModelAdmin):
	list_display = ('id','name','address',)
	#list_filter = (,)
	search_fields = ('name','address',)
	ordering = ('name','address',)

admin.site.register(Store,StoreAdmin)